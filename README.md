---
Welcome to the workshop, find below a list of the things we'll do during the workshop.

Steps:
- Create a new project in Gitlab and clone Arthur
- Start adding a job to execute flake in every commit
- Add a new job to add the unit tests. The two jobs will be assigned to different stages. In this step
we will filter the changes to be executed only with changes applied to master or via merge request.
- Add a new job to execute the tool radon, so we can check whether the complexity of the code is good or not
- Once all jobs are done, generate a Docker container
